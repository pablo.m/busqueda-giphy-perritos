import type { SearchRequestData, SearchResponseData } from '@/types/search.api.types';

// La llave de la API NUNCA debería exponerse, pero ésta fue creada con fines de prueba y meramente demostrativos
export const API_KEY = 'q5ijnfUEOGilDLqgjhjMswAwR1CFQyKe';

const searchGIF = async(data: SearchRequestData): Promise<SearchResponseData> => {
  const stringifiedData = {
    ...data,
    limit: String(data.limit),
    offset: String(data.offset)
  };
  const search = new URLSearchParams(stringifiedData).toString();

  try {
    const response = await fetch(`https://api.giphy.com/v1/gifs/search?${search}`);

    if (!response.ok) {
      throw new Error(response.statusText);
    }

    const responseJson = await response.json();

    return responseJson as SearchResponseData;
  } catch (error) {
    throw new Error(error as string);
  }
};

export default searchGIF;
