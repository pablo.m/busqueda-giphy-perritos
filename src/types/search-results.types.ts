import { SearchResponseData } from './search.api.types';

declare module '@vue/runtime-core' {
  export interface ComponentCustomProperties {
    gifData: SearchResponseData
  }
}

export type RecentSearch = {
  id: string | number;
  query: string;
  faved: boolean;
}
