/* eslint-disable camelcase */
export type SearchRequestData = {
  // eslint-disable-next-line camelcase
  api_key: string;
  q: string;
  limit?: number;
  offset?: number;
  rating?: 'g' | 'pg' | 'pg-13' | 'r';
  lang?: 'en'
    | 'es'
    | 'pt'
    | 'id'
    | 'fr'
    | 'ar'
    | 'tr'
    | 'th'
    | 'vi'
    | 'de'
    | 'it'
    | 'ja'
    | 'zh-CN'
    | 'zh-TW'
    | 'ru'
    | 'ko'
    | 'pl'
    | 'nl'
    | 'ro'
    | 'hu'
    | 'sv'
    | 'cs'
    | 'hi'
    | 'bn'
    | 'da'
    | 'fa'
    | 'tl'
    | 'fi'
    | 'he'
    | 'ms'
    | 'no'
    | 'uk';
  random_id?: string;
  bundle?: string;
}

type GIFUploaderUser = {
  avatar_url: string;
  banner_url: string;
  profile_url: string;
  username: string;
  display_name: string;
}

type StandardImageProperties = {
  url: string;
  width: string;
  height: string;
}

type WebpImageProperties = {
  webp: string;
  webp_size: string;
}

type Mp4ImageProperties = {
  mp4: string;
  mp4_size: string;
}

type GIFImages = {
  fixed_height: {
    size: string;
  } & StandardImageProperties & WebpImageProperties & Mp4ImageProperties;
  fixed_height_still: StandardImageProperties;
  fixed_height_downsampled: {
    size: string;
  } & StandardImageProperties & WebpImageProperties;
  fixed_width: {
    size: string;
  } & StandardImageProperties & WebpImageProperties & Mp4ImageProperties;
  fixed_width_still: StandardImageProperties;
  fixed_width_downsampled: {
    size: string;
  } & StandardImageProperties & WebpImageProperties;
  fixed_height_small: {
    size: string;
  } & StandardImageProperties & WebpImageProperties & Mp4ImageProperties;
  fixed_height_small_still: StandardImageProperties;
  fixed_width_small: {
    size: string;
  } & StandardImageProperties & WebpImageProperties & Mp4ImageProperties;
  fixed_width_small_still: StandardImageProperties;
  downsized: {
    size: string;
  } & StandardImageProperties;
  downsized_still: StandardImageProperties;
  downsized_large: {
    size: string;
  } & StandardImageProperties;
  downsized_medium: {
    size: string;
  } & StandardImageProperties;
  downsized_small: {
    size: string;
  } & StandardImageProperties;
  original: {
    frames: string;
    size: string;
  } & StandardImageProperties & WebpImageProperties & Mp4ImageProperties;
  original_still: StandardImageProperties;
  looping: {
    mp4: string;
  }
  preview: {
    width: string;
    height: string;
  } & Mp4ImageProperties;
  preview_gif: StandardImageProperties;
}

export type GIFData = {
  type: string;
  id: string;
  slug: string;
  url: string
  bitly_url: string;
  embed_url: string;
  username: string;
  source: string;
  rating: string;
  content_url: string;
  user: GIFUploaderUser;
  source_tld: string;
  source_post_url: string;
  update_datetime: string;
  create_datetime: string;
  import_datetime: string;
  trending_datetime: string;
  images: GIFImages;
  title: string;
}

type SearchPagination = {
  offset: number;
  total_count: number;
  count: number;
}

type SearchMeta = {
  msg: string;
  status: number;
  response_id: string;
}

export type SearchResponseData = {
  data: GIFData[];
  pagination: SearchPagination;
  meta: SearchMeta;
}
