import { createApp } from 'vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faGitlab } from '@fortawesome/free-brands-svg-icons';
import { faSearch, faBars, faTimes, faHeart, faStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as faEmptyStar } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import App from './App.vue';
import router from './router';
import './index.css';

library.add(faSearch, faBars, faTimes, faGitlab, faHeart, faStar, faEmptyStar);

const app = createApp(App);

app.component('font-awesome-icon', FontAwesomeIcon);
app.use(router);
app.mount('#app');
